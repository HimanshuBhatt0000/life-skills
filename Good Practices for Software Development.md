
### Which point(s) were new to you?

Using tools like loom for screencasts and Codepen or Codesandbox for sharing the entire setup when asking questions.
The recommendation to join meetings 5-10 minutes early to have some informal time with team members for better communication.


### Which area do you think you need to improve on? What are your ideas to make progress in that area?

I believe I can improve in the area of over-communication and always keeping my full attention on tasks. To make progress in this area, I plan to:

Set specific times for checking messages and instagram, avoiding distractions during focused work hours.
Implement the "work when you work, play when you play" rule to ensure I maintain full involvement in my tasks during work hours.
Prioritize maintaining a healthy food and exercise routine to ensure sustained energy and focus throughout the day.
