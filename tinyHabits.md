
### 1. What was the most interesting idea in the video?
Instead of trying to make big, overwhelming changes, you begin with very small actions that are easy to integrate into your daily routine. These tiny habits can serve as building blocks for larger, positive changes over time.

### 2. How can you use B = MAP to make making new habits easier? What are M, A, and P?
B MAP is Behavior equals Motivation, Ability, and Prompt. To make new habits easier, you can use this formula by focusing on 

- M (Motivation): Make the habit more appealing by connecting it to something you genuinely care about or find rewarding.
- A (Ability): Simplify the habit by breaking it down into smaller, manageable steps. Lower the barriers to entry so that it's easy to get started.
- P (Prompt): Create clear and specific cues or prompts that remind you to perform the habit. Make the cue obvious and easy to notice.

### 3. What was the most interesting story or idea for you?
The most interesting idea is the concept of starting with "tiny habits" and how it can lead to significant positive changes.start with something you can do.

### 4. What is the book's perspective about Identity?
it's not necessary to change your core identity to build new habits. You can start by making small behavioral changes without altering your self-concept. Over time, as you successfully make these changes into your life, your identity can naturally evolve to align with your new habits.

### 5. Write about the book's perspective on how to make a habit easier to do.
To make a habit easier to do, the book emphasizes starting with tiny, manageable steps and connecting the habit to something you find motivating or rewarding. Additionally, making the cue for the habit obvious and creating a supportive environment can make it easier to follow through with the habit.

### 6. Write about the book's perspective on how to make a habit harder to do.
To make a habit harder to do, you can increase the friction associated with the behavior. For example, if you want to reduce screen time, you can log out of social media apps, delete them from your phone, or set up website blockers to make access more difficult.

### 7. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
One habit I would like to do more of is reading. To make it more attractive and easy, I can:

- Set a specific time each day for reading (cue).
- Keep a book within easy reach.
- Choose books that genuinely interest me (motivation).

### 8. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
One habit I would like to eliminate or do less of is excessive snacking. To make it less attractive and harder, I can:

- Store unhealthy snacks out of sight or replace them with healthier options (make the cue invisible).
- Make a rule to eat only in the kitchen or dining area (change the environment to make it less convenient).
- Create a checklist of reasons to avoid excessive snacking (shift motivation away from it).
