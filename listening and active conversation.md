### What are the steps/strategies to do Active Listening? (Minimum 6 points)

    make myself aware of what he meant ,when he/she is speaking
    Avoid getting distracted by my own thoughts 
    Let him finish his talk first then speak
    i will use the words that ensure them i am intrested , like tell me more , thats intresting 
    mentally keeping the track of main points he/she said 
    show with my body language that i am listening to  what you said

### According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)
 Reflective Listening in Fisher's model is about non-judgmental listening, understanding the other ones perspective of why he/she think or do so, seeking clarification, and validating the other person's feelings and thoughts to get effective communication and understanding.

 ### What are the obstacles in your listening process?
 Well it depends , the intrest in the topic of what he/she is saying , the current problems of mine , and the trust and respect in person i am with , like if i don't trust him/her like a stranger then i am quite careful and thought of hidden agendas or hidden purpose of what is being said .

 ### What can you do to improve your listening?
 Work on my personal progress and made time for some human development phases like reading some books for extra perspectives, spending time with others to understand their way of living and thinking , that made me improve my listening skills or understanding skills in others topic .

 ### When do you switch to Passive communication style in your day to day life?
 i am always passive in communication and that leads to self esteeem problem i agree .

 ### When do you switch into Aggressive communication styles in your day to day life?
 almost none , i always try to be in a calm down situation even if meant to be compromise out of the line , a little

 ### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
 Well when i want to say something that might hurt or make someone angry but i know they are wrong i use sarcasm , i use gossiping if i think that will make the time a little better and fun but have my limits of what i will say , silent treatment i litreally do with people that misbehave or hurt my efforts for their selfishness or they don't try to understand that i too made part in compromise, or i generally do it when i don't want to be connected to that person anymore.
 
