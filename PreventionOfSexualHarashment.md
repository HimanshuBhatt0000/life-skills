### Explains different scenarios enacted by actors.

Well, there are some scenarios like:

1. Asking for a favor again and again, even when the person said no, favor like going out on a date.
2. Something on your computer, jokes, cartoons that are considered to be sexual in nature.
3. A person has a habit of greeting other colleagues with hugs, and it is not considered comfortable by some employees.
4. An employee getting treated well, accounted well because she looks good or she is beautiful.
5. A female employee uses her charm of talking more, or flirting to get a favor at work.
6. A person judges a female employee's work by her female nature and comments like it might be easy for her.
7. A person commenting on a female employee's dress every day, he said it's good.

### How to handle cases of harassment?

It depends on the scenario I am in. First, make myself seem uninterested and not be part of the situation. If the behavior is still not corrected, I simply make a statement to him/her that I am uncomfortable with their behavior.

### How to behave appropriately?

I like one point where it says to strangers/colleagues you don't know very well, only say things that you might consider okay if your parents, your spouse, your children are standing next to you. Also, pay attention to whether the other person is comfortable or showing signs of discomfort.
